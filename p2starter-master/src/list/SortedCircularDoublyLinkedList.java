package list;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class SortedCircularDoublyLinkedList<E extends Comparable<E>> implements SortedList<E> {

	private Node<E> header;
	private int currentSize;

	public SortedCircularDoublyLinkedList() {
		header = new Node<E>();
		header.setNext(header);
		header.setPrev(header);
		currentSize = 0;
	}

	@Override
	public boolean add(E object) {
		Node<E> newNode = new Node<E>(object);
		Node<E> node = header;
		if (size() == 0) {
			header.setNext(newNode);
			header.setPrev(newNode);
			newNode.setNext(header);
			newNode.setPrev(header);
			currentSize++;
			return true;
		} else {
			node = node.getPrev();
			for (int i = currentSize; i >= 0; i--) {
				if (node.getElement() == null || node.getElement().compareTo(object) < 0
						|| node.getElement().compareTo(object) == 0) {
					newNode.setNext(node.getNext());
					newNode.setPrev(node);
					node.getNext().setPrev(newNode);
					node.setNext(newNode);
					currentSize++;
					return true;
				}
				node = node.getPrev();
			}
		}
		return false;
	}

	@Override
	public int size() {
		return currentSize;
	}

	@Override
	public boolean remove(E object) {
		Node<E> node = header.getNext();
		for (int i = 0; i < currentSize; i++) {
			if (node.getElement().compareTo(object) == 0) {
				removeNode(node);
				return true;
			}
			node = node.getNext();
		}
		return false;
	}

	@Override
	public boolean remove(int index) throws IndexOutOfBoundsException {
		if (index < 0 || index >= currentSize)
			throw new IndexOutOfBoundsException("index invalid");
		Node<E> node = header.getNext();
		for (int i = 0; i < index; i++) {
			node = node.getNext();
		}
		removeNode(node);
		return true;
	}

	@Override
	public int removeAll(E object) {
		int count = 0;
		Node<E> node = header.getNext();
		Node<E> nextNode;
		for (int i = 0; i < currentSize; i++) {
			nextNode = node.getNext();
			if (node.getElement().compareTo(object) == 0) {
				removeNode(node);
				i--;
			}
			node = nextNode;
		}
		return count;
	}

	public void removeNode(Node<E> node) {
		Node<E> pNode = node.getPrev();
		Node<E> nNode = node.getNext();
		pNode.setNext(nNode);
		nNode.setPrev(pNode);
		currentSize--;
		node.cleanNode();
	}

	@Override
	public E first() {
		if (size() == 0) {
			return null;
		}
		return header.getNext().getElement();
	}

	@Override
	public E last() {
		if (size() == 0) {
			return null;
		}
		return header.getPrev().getElement();
	}

	@Override
	public E get(int index) throws IndexOutOfBoundsException {
		if (index < 0 || index >= currentSize)
			throw new IndexOutOfBoundsException("index invalid");
		Node<E> node = header.getNext();
		for (int i = 0; i < index; i++) {
			node = node.getNext();
		}
		return node.getElement();
	}

	@Override
	public E set(int index, E object) throws IndexOutOfBoundsException {
		if (index < 0 || index >= currentSize)
			throw new IndexOutOfBoundsException("index invalid");
		Node<E> node = header.getNext();
		for (int i = 0; i < index; i++) {
			node = node.getNext();
		}
		E oldValue = node.getElement();
		node.setElement(object);
		return oldValue;
	}

	@Override
	public void clear() {
		Node<E> node = header.getNext();
		Node<E> nextNode;
		for (int i = 0; i < currentSize; i++) {
			nextNode = node.getNext();
			removeNode(node);
			node = nextNode;
			i--;
		}
	}

	@Override
	public boolean contains(E object) {
		Node<E> node = header.getNext();
		for (int i = 0; i < currentSize; i++) {
			if (node.getElement().compareTo(object) == 0) {
				return true;
			}
			node = node.getNext();
		}
		return false;
	}

	@Override
	public boolean isEmpty() {
		if (size() == 0) {
			return true;
		}
		return false;
	}

	@Override
	public int firstIndex(E object) {
		Node<E> node = header.getNext();
		for (int i = 0; i < currentSize; i++) {
			if (node.getElement().compareTo(object) == 0) {
				return i;
			}
			node = node.getNext();
		}
		return -1;
	}

	@Override
	public int lastIndex(E object) {
		Node<E> node = header.getPrev();
		for (int i = currentSize - 1; i >= 0; i--) {
			if (node.getElement().compareTo(object) == 0) {
				return i;
			}
			node = node.getPrev();
		}
		return -1;
	}

	@Override
	public Iterator<E> iterator() {
		return new forwardIterator();
	}

	@Override
	public Iterator<E> iterator(int index) throws IndexOutOfBoundsException {
		if (index < 0 || index >= currentSize)
			throw new IndexOutOfBoundsException("index invalid");
		return new forwardIterator(index);
	}

	@Override
	public ReverseIterator<E> reverseIterator() {
		return new reverseIterator();
	}

	@Override
	public ReverseIterator<E> reverseIterator(int index) throws IndexOutOfBoundsException {
		if (index < 0 || index >= currentSize)
			throw new IndexOutOfBoundsException("index invalid");
		return new reverseIterator(index);
	}

	private static class Node<T> {
		private T data;
		private Node<T> prev;
		private Node<T> next;

		public Node() {
			data = null;
			prev = null;
			next = null;
		}

		public Node(T data) {
			this.data = data;
			prev = null;
			next = null;
		}

		public T getElement() {
			return data;
		}

		public void setElement(T data) {
			this.data = data;
		}

		public Node<T> getPrev() {
			return prev;
		}

		public void setPrev(Node<T> prev) {
			this.prev = prev;
		}

		public Node<T> getNext() {
			return next;
		}

		public void setNext(Node<T> next) {
			this.next = next;
		}

		public void cleanNode() {
			data = null;
			prev = null;
			next = null;
		}
	}

	private class forwardIterator implements Iterator<E> {
		private Node<E> current;

		public forwardIterator() {
			current = header;
		}

		public forwardIterator(int index) {
			for (int i = 0; i <= index; i++) {
				current = current.getNext();
			}
		}

		public boolean hasNext() {
			return current.getNext() != header;
		}

		public E next() throws NoSuchElementException {
			if (!hasNext())
				throw new NoSuchElementException("The iterator has finished.");
			current = current.getNext();
			E etr = current.getElement();
			return etr;
		}

		public void remove() throws UnsupportedOperationException, IllegalStateException {
			throw new UnsupportedOperationException("Remove is not implemented.");
		}
	}

	private class reverseIterator implements ReverseIterator<E> {
		private Node<E> current;

		public reverseIterator() {
			current = header;
		}

		public reverseIterator(int index) {
			for (int i = 0; i <= index; i++) {
				current = current.getPrev();
			}
		}

		@Override
		public boolean hasPrevious() {
			return current.getPrev() != header;
		}

		@Override
		public E previous() throws NoSuchElementException {
			if (!hasPrevious())
				throw new NoSuchElementException("The iterator has finished.");
			current = current.getPrev();
			E etr = current.getElement();
			return etr;
		}
	}

}