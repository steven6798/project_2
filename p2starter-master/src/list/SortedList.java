package list;

import java.util.Iterator;

public interface SortedList<E extends Comparable<E>> extends Iterable<E> {

	/**
	 * Adds a new element to the list in the right order.
	 * 
	 * @param object the object to add.
	 * @return true
	 */
	public boolean add(E object);

	/**
	 * Return the number of elements in the list.
	 * 
	 * @return the number of elements in the list.
	 */
	public int size();

	/**
	 * Removes the first occurrence of object from the list.
	 * 
	 * @param object the object to remove.
	 * @return true if erased, or false otherwise.
	 */
	public boolean remove(E object);

	/**
	 * Removes the elements at position index from the list.
	 * 
	 * @param index the position of the element to remove.
	 * @return true if the element is erased.
	 * @throws IndexOutOfBoundsException if index is illegal.
	 */
	public boolean remove(int index) throws IndexOutOfBoundsException;

	/**
	 * Removes all copies of element object from the list.
	 * 
	 * @param object the object to remove.
	 * @return the number of copies erased.
	 */
	public int removeAll(E object);

	/**
	 * Returns the first element in the list.
	 * 
	 * @return the first element in the list, or null if the list is empty.
	 */
	public E first();

	/**
	 * Returns the last element in the list.
	 * 
	 * @return the last element in the list, or null if the list is empty.
	 */
	public E last();

	/**
	 * Returns the element at position index.
	 * 
	 * @param index the position of the element to return.
	 * @return the elements at position index.
	 * @throws IndexOutOfBoundsException if index is illegal.
	 */
	public E get(int index) throws IndexOutOfBoundsException;

	/**
	 * Assign a new value to the element at position index.
	 * 
	 * @param index  the position of the element.
	 * @param object the new value to assign.
	 * @return the old value of the element at position index.
	 * @throws IndexOutOfBoundsException if index is illegal.
	 */
	public E set(int index, E object) throws IndexOutOfBoundsException;

	/**
	 * Removes all elements in the list.
	 */
	public void clear();

	/**
	 * Returns true if the element e is in the list or false otherwise.
	 * 
	 * @param e the element to find.
	 * @return true if the element e is in the list, false otherwise.
	 */
	public boolean contains(E e);

	/**
	 * Returns true if the list is empty or false otherwise.
	 * 
	 * @return true if the list is empty, false otherwise.
	 */
	public boolean isEmpty();

	/**
	 * Returns a forward iterator starting from position index in the list.
	 * 
	 * @param index the position where the forward iterator will start.
	 * @return a forward iterator from position index.
	 * @throws IndexOutOfBoundsException if index is illegal.
	 */
	public Iterator<E> iterator(int index) throws IndexOutOfBoundsException;

	/**
	 * Returns the index of the first position of element e in the list.
	 * 
	 * @param e the element to return.
	 * @return the index of the first position of element e in the list or -1 if the
	 *         element is not present.
	 */
	public int firstIndex(E e);

	/**
	 * Returns the index of the last position of element e in the list.
	 * 
	 * @param e the element to return.
	 * @return the index of the last position of element e in the list or -1 if the
	 *         element is not present.
	 */
	public int lastIndex(E e);

	/**
	 * Returns a reverse iterator starting from the last element in the list.
	 * 
	 * @return a reverse iterator starting from the last element in the list.
	 */
	public ReverseIterator<E> reverseIterator();

	/**
	 * Returns a reverse iterator starting from position index in the list.
	 * 
	 * @param index the position where the reverse iterator will start.
	 * @return a reverse iterator from position index.
	 * @throws IndexOutOfBoundsException if index is illegal.
	 */
	public ReverseIterator<E> reverseIterator(int index) throws IndexOutOfBoundsException;

}